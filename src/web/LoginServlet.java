package web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;  
import javax.servlet.http.HttpServletRequest;  
import javax.servlet.http.HttpServletResponse;  
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

import dao.UserDAO;
import model.UserModel;

@WebServlet("/login")

public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private UserDAO userDAO;
	
	public void init() {
		userDAO = new UserDAO();
	}
	
    protected void doPost(HttpServletRequest request, HttpServletResponse response)  
                    throws ServletException, IOException {  
        
    	String email = request.getParameter("email");  
        String password = request.getParameter("password");  
        
        UserModel user = userDAO.selectUserByEmail(email, password);
		//System.out.println(user);
		
        boolean responseData = false;
        
    	if(user != null){
        	
        	HttpSession session = request.getSession();  
	        session.setAttribute("user_id", user.getId());
	        session.setAttribute("name", user.getName());
	        session.setAttribute("email", user.getEmail());
	        session.setAttribute("photo", user.getPhoto());
	        session.setAttribute("usertype", user.getUserType());
	        
	        responseData = true;	
        }
    	
    	String json = new Gson().toJson(responseData);

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(json);
        
    }
    
    
}  