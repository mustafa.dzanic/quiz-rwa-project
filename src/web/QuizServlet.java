package web;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import com.google.gson.Gson;

import dao.QuizDAO;
import model.QuizModel;

@WebServlet("/quiz/*")
@MultipartConfig
public class QuizServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private QuizDAO quizDAO;
	
	public void init() {
		quizDAO = new QuizDAO();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		doGet(request, response);
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String action = request.getPathInfo().substring(1);
		
		try {
			switch (action) {
			case "create":
				createQuiz(request, response);
				break;
			case "getRandom":
				getRandomQuiz(request, response);
				break;
			case "list":
				listQuiz(request, response);
				break;
			case "update":
				updateQuiz(request, response);
				break;
			case "delete":
				deleteQuiz(request, response);
			case "getOnlineUsers":
				getOnlineUsers(request, response);
				break;
			}
		} catch (SQLException ex) {
			throw new ServletException(ex);
		}
	}

	private void createQuiz(HttpServletRequest request, HttpServletResponse response) 
			throws SQLException, IOException, ServletException {
		
		Part filePart = request.getPart("file");
	    String fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString();
	    InputStream fileContent = filePart.getInputStream();
	    
	    String json = request.getParameter("obj");
		QuizModel newQuiz = new Gson().fromJson(json, QuizModel.class);
		newQuiz.setImage(fileName);
		
		boolean responseData = quizDAO.createQuiz(newQuiz);		
		json = new Gson().toJson(responseData);
		
		String relativeWebPath = "/uploads";
		String absoluteDiskPath = getServletContext().getRealPath(relativeWebPath);
		
		File file = new File(absoluteDiskPath, fileName);
	    Files.copy(fileContent, file.toPath(), StandardCopyOption.REPLACE_EXISTING);
	    
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(json);
        
	}
	
	/*private static String getFileExtension(String fileName) {
        if(fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0)
        return fileName.substring(fileName.lastIndexOf(".")+1);
        else return "";
    }*/
	
	private void listQuiz(HttpServletRequest request, HttpServletResponse response)
			throws SQLException, IOException, ServletException {
		List <QuizModel> listQuiz = quizDAO.selectAllQuizs();
		
		String json = new Gson().toJson(listQuiz);

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(json);
	}

	private void getRandomQuiz(HttpServletRequest request, HttpServletResponse response)
			throws SQLException, IOException, ServletException {
		
		QuizModel getRandomQuiz = quizDAO.selectRandomQuiz();
		
		String json = new Gson().toJson(getRandomQuiz);

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(json);
	}
	
	private void updateQuiz(HttpServletRequest request, HttpServletResponse response) 
			throws SQLException, IOException, ServletException {
		Part filePart = request.getPart("file");
	    String fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString();
	    InputStream fileContent = filePart.getInputStream();
	    
	    String json = request.getParameter("obj");
		QuizModel updateQuiz = new Gson().fromJson(json, QuizModel.class);
		updateQuiz.setImage(fileName);
		
		boolean responseData = quizDAO.updateQuiz(updateQuiz);		
		json = new Gson().toJson(responseData);
		
		String relativeWebPath = "/uploads";
		String absoluteDiskPath = getServletContext().getRealPath(relativeWebPath);
		
		File file = new File(absoluteDiskPath, fileName);
	    Files.copy(fileContent, file.toPath(), StandardCopyOption.REPLACE_EXISTING);
	    
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(json);
	}

	private void deleteQuiz(HttpServletRequest request, HttpServletResponse response) 
			throws SQLException, IOException {
		int id = Integer.parseInt(request.getParameter("id"));
		
		boolean responseData = quizDAO.deleteQuiz(id);	
		String json = new Gson().toJson(responseData);

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(json);

	}
	
	private void getOnlineUsers(HttpServletRequest request, HttpServletResponse response) 
			throws SQLException, IOException {
		
		HttpSession session = request.getSession();		
		int users = quizDAO.getOnlineUsers(session.getId());	
		String json = new Gson().toJson(users);

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(json);

	}

}
