package web;


import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;  
import javax.servlet.http.HttpServletRequest;  
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

import dao.UserDAO;
import model.UserModel;

@WebServlet("/signup")

public class SignupServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private UserDAO userDAO;
	
	public void init() {
		userDAO = new UserDAO();
	}
	
    protected void doPost(HttpServletRequest request, HttpServletResponse response)  
                    throws ServletException, IOException {  
    	System.out.println("Signup");
    	try {
    		
    		signUpUser(request, response);  
    		
    	} catch (SQLException e) {
    		
    	}
        
    }
    
    private void signUpUser(HttpServletRequest request, HttpServletResponse response) 
			throws SQLException, IOException {
		String name = request.getParameter("name");
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		UserModel newUser = new UserModel(name, email, password);
		
		boolean responseData = userDAO.createUser(newUser);		
		
		if(responseData == true){
        	
        	HttpSession session = request.getSession();  
        	//session.setAttribute("user_id", user.getId());
	        session.setAttribute("name", name);
	        session.setAttribute("email", email);
	        session.setAttribute("photo", "");
	        session.setAttribute("usertype", "user");
	
        }
    	
		String json = new Gson().toJson(responseData);

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(json);
        
	}
}  