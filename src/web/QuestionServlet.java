package web;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import dao.QuestionDAO;
import model.QuestionModel;

@WebServlet("/question/*")
@MultipartConfig
public class QuestionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private QuestionDAO questionDAO;
	
	public void init() {
		questionDAO = new QuestionDAO();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		doGet(request, response);
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String action = request.getPathInfo().substring(1);
		
		try {
			switch (action) {
			case "create":
				createQuestion(request, response);
				break;
			case "getRandom":
				getRandomQuestion(request, response);
				break;
			case "list":
				listQuestion(request, response);
				break;
			case "update":
				updateQuestion(request, response);
				break;
			case "delete":
				deleteQuestion(request, response);
				break;
			}
		} catch (SQLException ex) {
			throw new ServletException(ex);
		}
	}

	private void createQuestion(HttpServletRequest request, HttpServletResponse response) 
			throws SQLException, IOException, ServletException {
		
		int quiz_id = Integer.parseInt(request.getParameter("quiz_id"));
		String question = request.getParameter("question");
		String answer = request.getParameter("answer");
		String correct_answer = request.getParameter("correct_answer");
		int time_limit = Integer.parseInt(request.getParameter("time_limit"));
		
		QuestionModel newQuestion = new QuestionModel(quiz_id, question, answer, correct_answer, time_limit);
		
		boolean responseData = questionDAO.createQuestion(newQuestion);		
		String json = new Gson().toJson(responseData);
		
		response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(json);
        
	}
	
	private void listQuestion(HttpServletRequest request, HttpServletResponse response)
			throws SQLException, IOException, ServletException {
		int quiz_id = Integer.parseInt(request.getParameter("quiz_id"));
		List <QuestionModel> listQuestion = questionDAO.selectAllQuestions(quiz_id);
		
		String json = new Gson().toJson(listQuestion);

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(json);
	}

	private void getRandomQuestion(HttpServletRequest request, HttpServletResponse response)
			throws SQLException, IOException, ServletException {
		
		int quiz_id = Integer.parseInt(request.getParameter("quiz_id"));
		int questions_number = Integer.parseInt(request.getParameter("questions_number"));
		
		List <QuestionModel> getRandomQuestion = questionDAO.selectRandomQuestions(quiz_id, questions_number);
		
		String json = new Gson().toJson(getRandomQuestion);

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(json);
	}
	
	private void updateQuestion(HttpServletRequest request, HttpServletResponse response) 
			throws SQLException, IOException, ServletException {
		
		int id = Integer.parseInt(request.getParameter("id"));
		int quiz_id = Integer.parseInt(request.getParameter("quiz_id"));
		String question = request.getParameter("question");
		String answer = request.getParameter("answer");
		String correct_answer = request.getParameter("correct_answer");
		int time_limit = Integer.parseInt(request.getParameter("time_limit"));
		
		QuestionModel updateQuestion = new QuestionModel(id, quiz_id, question, answer, correct_answer, time_limit);
		
		boolean responseData = questionDAO.updateQuestion(updateQuestion);		
		String json = new Gson().toJson(responseData);
		
		response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(json);
	}

	private void deleteQuestion(HttpServletRequest request, HttpServletResponse response) 
			throws SQLException, IOException {
		int id = Integer.parseInt(request.getParameter("id"));
		
		boolean responseData = questionDAO.deleteQuestion(id);	
		String json = new Gson().toJson(responseData);

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(json);

	}

}
