package web;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.activation.MimetypesFileTypeMap;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import dao.QuizResultDAO;
import model.QuizResultModel;

@WebServlet("/quizresult/*")
public class QuizResultServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private QuizResultDAO quizResultDAO;
	
	public void init() {
		quizResultDAO = new QuizResultDAO();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		doGet(request, response);
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String action = request.getPathInfo().substring(1);
		
		try {
			switch (action) {
			case "create":
				createQuizResult(request, response);
				break;
			case "list":
				listQuizResult(request, response);
				break;
			case "update":
				updateQuizResult(request, response);
				break;
			case "delete":
				deleteQuizResult(request, response);
				break;
			case "convertCSV":
				convertQuizResultToCSV(request, response);
				break;
			case "downloadCSV":
				downloadCSV(request, response);
				break;
			}
		} catch (SQLException ex) {
			throw new ServletException(ex);
		}
	}

	private void createQuizResult(HttpServletRequest request, HttpServletResponse response) 
			throws SQLException, IOException {
		
		String user_name = request.getParameter("user_name");
		String user_email = request.getParameter("user_email");
		int quiz_id = Integer.parseInt(request.getParameter("quiz_id"));
		String correct_count = request.getParameter("correct_count");
		String answer_count = request.getParameter("answer_count");
		
		QuizResultModel newQuizResult = new QuizResultModel(user_name, user_email, quiz_id, correct_count, answer_count);
		
		boolean responseData = quizResultDAO.createQuizResult(newQuizResult);		
		String json = new Gson().toJson(responseData);

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(json);
        
	}
	
	private void convertQuizResultToCSV(HttpServletRequest request, HttpServletResponse response) 
			throws SQLException, IOException {
		
		String relativeWebPath = "/csv";
		String absoluteDiskPath = getServletContext().getRealPath(relativeWebPath);
		
		DateTimeFormatter FOMATTER = DateTimeFormatter.ofPattern("MM-dd-yyyy-hh-mm-a");		 
		ZonedDateTime zdt = ZonedDateTime.now();
		String zdtString = FOMATTER.format(zdt);
		
		String fileName = "inbox.csv";
		
		File file = new File(absoluteDiskPath, fileName);
		PrintWriter pw= new PrintWriter(file);
	    StringBuilder sb=new StringBuilder();
	 
	    List <QuizResultModel> listQuizResult = quizResultDAO.selectAllQuizResults();
		
	    sb.append("No");
	    sb.append(","); 
		sb.append("Name");
	    sb.append(","); 
		sb.append("E-mail");
	    sb.append(","); 
		sb.append("Quiz Title");
	    sb.append(","); 
		sb.append("Correct");
	    sb.append(","); 
		sb.append("Total");
	    sb.append(","); 
		sb.append("Date");
		sb.append("\r\n");
		
		int cnt = 0;
	    for (QuizResultModel quiz : listQuizResult) {
	    	cnt++;
			sb.append(cnt);
		    sb.append(","); 
			sb.append(quiz.getUserName());
		    sb.append(","); 
			sb.append(quiz.getUserEmail());
		    sb.append(","); 
			sb.append(quiz.getQuizTitle());
		    sb.append(","); 
			sb.append(quiz.getAnswerCount());
		    sb.append(","); 
		    sb.append(quiz.getCorrectCount());
		    sb.append(","); 
			sb.append(quiz.getCreateAt());
			sb.append("\r\n");
	    }
	    
		pw.write(sb.toString());
	    pw.close();
	    
	    System.out.println("Converted");
	    
	    boolean responseData = true;		
		String json = new Gson().toJson(responseData);

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(json);
        
	}
	
	private void downloadCSV(HttpServletRequest request, HttpServletResponse response) 
			throws SQLException, IOException {
		
		String relativeWebPath = "/csv";
		String absoluteDiskPath = getServletContext().getRealPath(relativeWebPath);
		String fileName = "inbox.csv";
		
		File file = new File(absoluteDiskPath, fileName);		
	    
	    MimetypesFileTypeMap mimeTypesMap = new MimetypesFileTypeMap();
        String mimeType = mimeTypesMap.getContentType(file);
        System.out.println(mimeType);
        
        response.setContentType(mimeType);
        response.setHeader("Content-disposition", "attachment; filename=" + fileName);

        OutputStream out = response.getOutputStream();
        FileInputStream in = new FileInputStream(file);
        byte[] buffer = new byte[4096];
        int length;
        while ((length = in.read(buffer)) > 0) {
            out.write(buffer, 0, length);
        }
        in.close();
        out.flush();        
	    
        System.out.println("Download");        
        
	}
	
	private void listQuizResult(HttpServletRequest request, HttpServletResponse response)
			throws SQLException, IOException, ServletException {
		List <QuizResultModel> listQuizResult = quizResultDAO.selectAllQuizResults();
		
		String json = new Gson().toJson(listQuizResult);

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(json);
	}

	private void updateQuizResult(HttpServletRequest request, HttpServletResponse response) 
			throws SQLException, IOException {
		
		int id = Integer.parseInt(request.getParameter("id"));
		String user_name = request.getParameter("user_name");
		String user_email = request.getParameter("user_email");
		int quiz_id = Integer.parseInt(request.getParameter("quiz_id"));
		String correct_count = request.getParameter("correct_count");
		String answer_count = request.getParameter("answer_count");
		
		QuizResultModel quizResult = new QuizResultModel(id, user_name, user_email, quiz_id, correct_count, answer_count);
		
		boolean responseData = quizResultDAO.updateQuizResult(quizResult);	
		String json = new Gson().toJson(responseData);

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(json);
	}

	private void deleteQuizResult(HttpServletRequest request, HttpServletResponse response) 
			throws SQLException, IOException {
		int id = Integer.parseInt(request.getParameter("id"));
		
		boolean responseData = quizResultDAO.deleteQuizResult(id);	
		String json = new Gson().toJson(responseData);

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(json);

	}

}
