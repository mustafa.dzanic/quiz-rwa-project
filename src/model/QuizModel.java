package model;

public class QuizModel {
	protected int id;
	protected int user_id;
	protected String title;
	protected String image;
	
	public QuizModel() {
		
	}
	
	public QuizModel(int user_id, String title, String image) {
		
		super();
		this.user_id = user_id;
		this.title = title;
		this.image = image;
		
	}

	public QuizModel(int id, int user_id, String title, String image) {
		
		super();
		this.id = id;
		this.user_id = user_id;
		this.title = title;
		this.image = image;
		
	}

	public int getId() {
		
		return id;
		
	}
	
	public int getUserId() {
		
		return user_id;
		
	}

	public String getTitle() {
		
		return title;
		
	}
	
	public String getImage() {
		
		return image;
		
	}
	
	public void setImage(String image) {
		
		this.image = image;
		
	}

}

