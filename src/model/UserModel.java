package model;

public class UserModel {
	protected int id;
	protected String name;
	protected String email;
	protected String password;
	protected String photo;
	protected String usertype;
	
	public UserModel() {
		
	}
	
	public UserModel(String email, String password) {
		
		super();
		this.email = email;
		this.password = password;
		
	}
	
	public UserModel(String name, String email, String password) {
		
		super();
		this.name = name;
		this.email = email;
		this.password = password;
		
	}

	public UserModel(String name, String email, String photo, String usertype) {
		
		super();
		this.name = name;
		this.email = email;
		this.photo = photo;
		this.usertype = usertype;
		
	}
	
	public UserModel(int id, String name, String email, String password) {
		
		super();
		this.id = id;
		this.name = name;
		this.email = email;
		this.password = password;
		
	}
	
	public UserModel(int id, String name, String email, String password, String photo) {
		
		super();
		this.id = id;
		this.name = name;
		this.email = email;
		this.password = password;
		this.photo = photo;
		
	}

	public UserModel(int id, String name, String email, String password, String photo, String usertype) {
		
		super();
		this.id = id;
		this.name = name;
		this.email = email;
		this.password = password;
		this.photo = photo;
		this.usertype = usertype;
		
	}

	public int getId() {
		
		return id;
		
	}
	
	public void setId(int id) {
		
		this.id = id;
		
	}
	
	public String getName() {
		
		return name;
		
	}
	
	public void setName(String name) {
		
		this.name = name;
		
	}
	
	public String getEmail() {
		
		return email;
		
	}
	
	public void setEmail(String email) {
		
		this.email = email;
		
	}
	
	public String getPassword() {
		
		return password;
		
	}
	
	public void setPassword(String password) {
		
		this.password = password;
		
	}
	
	public String getPhoto() {
		
		return photo;
		
	}
	
	public String getUserType() {
		
		return usertype;
		
	}
	
}

