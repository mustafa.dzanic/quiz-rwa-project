package model;

public class QuizResultModel {
	protected int id;
	protected String user_name;
	protected String user_email;
	protected int quiz_id;
	protected String quiz_title;
	protected String correct_count;
	protected String answer_count;
	protected String create_at;
	
	public QuizResultModel() {
		
	}
	
	public QuizResultModel(int id, String user_name, String user_email, int quiz_id, String correct_count, String answer_count) {
		
		super();
		this.id = id;
		this.user_name = user_name;
		this.user_email = user_email;
		this.quiz_id = quiz_id;
		this.correct_count = correct_count;
		this.answer_count = answer_count;
		
	}
	
	public QuizResultModel(int id, String user_name, String user_email, int quiz_id, String quiz_title, String correct_count, String answer_count, String create_at) {
		
		super();
		this.id = id;
		this.user_name = user_name;
		this.user_email = user_email;
		this.quiz_id = quiz_id;
		this.quiz_title = quiz_title;
		this.correct_count = correct_count;
		this.answer_count = answer_count;
		this.create_at = create_at;
		
	}

	public QuizResultModel(String user_name, String user_email, int quiz_id, String correct_count, String answer_count) {
		
		super();
		this.user_name = user_name;
		this.user_email = user_email;
		this.quiz_id = quiz_id;
		this.correct_count = correct_count;
		this.answer_count = answer_count;
		
	}

	public int getId() {
		
		return id;
		
	}

	public String getUserName() {
		
		return user_name;
		
	}
	
	public String getUserEmail() {
		
		return user_email;
		
	}

	public int getQuizId() {
		
		return quiz_id;
		
	}
	
	public String getQuizTitle() {
		
		return quiz_title;
		
	}

	public String getCorrectCount() {
		
		return correct_count;
		
	}

	public String getAnswerCount() {
		
		return answer_count;
		
	}
	
	public String getCreateAt() {
		
		return create_at;
		
	}

}

