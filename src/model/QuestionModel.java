package model;

public class QuestionModel {
	protected int id;
	protected int quiz_id;
	protected String question;
	protected String answer;
	protected String correct_answer;
	protected int time_limit;
	
	public QuestionModel() {
		
	}
	
	public QuestionModel(int id, int quiz_id, String question, String answer, String correct_answer, int time_limit) {
		
		super();
		this.id = id;
		this.quiz_id = quiz_id;
		this.question = question;
		this.answer = answer;
		this.correct_answer = correct_answer;
		this.time_limit = time_limit;
		
	}
	
	public QuestionModel(int quiz_id, String question, String answer, String correct_answer, int time_limit) {
		
		super();
		this.quiz_id = quiz_id;
		this.question = question;
		this.answer = answer;
		this.correct_answer = correct_answer;
		this.time_limit = time_limit;
		
	}

	public int getId() {
		
		return id;
		
	}
	
	public int getQuizId() {
		
		return quiz_id;
		
	}

	public String getQuestion() {
		
		return question;
		
	}

	public String getAnswer() {
		
		return answer;
		
	}

	public String getCorrectAnswer() {
		
		return correct_answer;
		
	}
	
	public int getTimeLimit() {
		
		return time_limit;
		
	}

}

