package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.QuestionModel;

public class QuestionDAO {
	private String jdbcURL = "jdbc:mysql://localhost:3306/quiz?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    private String jdbcUsername = "root";
    private String jdbcPassword = "";

	private static final String INSERT_QUESTIONS_SQL 	= "INSERT INTO questions" + "  (quiz_id, question, answer, correct_answer, time_limit) VALUES " + " (?, ?, ?, ?, ?);";
	private static final String SELECT_QUESTION_BY_ID 	= "SELECT * FROM questions WHERE id = ?";
	private static final String SELECT_ALL_QUESTIONS 	= "SELECT * FROM questions WHERE quiz_id=?";
	private static final String SELECT_RANDOM_QUESTIONS = "SELECT * FROM questions WHERE quiz_id=? ORDER BY RAND() LIMIT ?";
	private static final String DELETE_QUESTIONS_SQL 	= "DELETE FROM questions WHERE id = ?;";
	private static final String UPDATE_QUESTIONS_SQL 	= "UPDATE questions SET quiz_id = ?, question = ?, answer = ?, correct_answer = ?, time_limit = ? WHERE id = ?;";

	public QuestionDAO() {
		
	}

	protected Connection getConnection() {
		Connection connection = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			connection = DriverManager.getConnection(jdbcURL, jdbcUsername, jdbcPassword);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return connection;
	}

	public boolean createQuestion(QuestionModel question) throws SQLException {
		boolean rowInserted = false;
		try (Connection connection = getConnection();
				PreparedStatement statement = connection.prepareStatement(INSERT_QUESTIONS_SQL)) {
			statement.setInt(1, question.getQuizId());
			statement.setString(2, question.getQuestion());
			statement.setString(3, question.getAnswer());
			statement.setString(4, question.getCorrectAnswer());
			statement.setInt(5, question.getTimeLimit());
			
			System.out.println(statement);
			rowInserted = statement.executeUpdate() > 0;
			
		} catch (SQLException e) {
			printSQLException(e);
		}
		return rowInserted;
	}

	public QuestionModel selectQuestion(int id) {
		QuestionModel questionModel = null;
		try (Connection connection = getConnection();
				PreparedStatement statement = connection.prepareStatement(SELECT_QUESTION_BY_ID);) {
			statement.setInt(1, id);
			
			System.out.println(statement);
			ResultSet rs = statement.executeQuery();

			while (rs.next()) {
				int quiz_id = rs.getInt("quiz_id");
				String question = rs.getString("question");
				String answer = rs.getString("answer");
				String correct_answer = rs.getString("correct_answer");
				int time_limit = rs.getInt("time_limit");
				questionModel = new QuestionModel(id, quiz_id, question, answer, correct_answer, time_limit);
			}
		} catch (SQLException e) {
			printSQLException(e);
		}
		return questionModel;
	}
	
	public List<QuestionModel> selectAllQuestions(int quiz_id) {

		List<QuestionModel> questionModels = new ArrayList<>();
		try (Connection connection = getConnection();
			PreparedStatement statement = connection.prepareStatement(SELECT_ALL_QUESTIONS);) {
			statement.setInt(1, quiz_id);
			
			ResultSet rs = statement.executeQuery();
			while (rs.next()) {
				int id = rs.getInt("id");
				String question = rs.getString("question");
				String answer = rs.getString("answer");
				String correct_answer = rs.getString("correct_answer");
				int time_limit = rs.getInt("time_limit");
				questionModels.add(new QuestionModel(id, quiz_id, question, answer, correct_answer, time_limit));
			}
		} catch (SQLException e) {
			printSQLException(e);
		}
		return questionModels;
	}

	public List<QuestionModel> selectRandomQuestions(int quiz_id, int questions_number) {
		List<QuestionModel> questionModels = new ArrayList<>();
		try (Connection connection = getConnection();
				PreparedStatement statement = connection.prepareStatement(SELECT_RANDOM_QUESTIONS);) {
			
			statement.setInt(1, quiz_id);			
			statement.setInt(2, questions_number);			
			System.out.println(statement);
			ResultSet rs = statement.executeQuery();

			while (rs.next()) {
				int id = rs.getInt("id");
				String question = rs.getString("question");
				String answer = rs.getString("answer");
				String correct_answer = rs.getString("correct_answer");
				int time_limit = rs.getInt("time_limit");
				questionModels.add(new QuestionModel(id, quiz_id, question, answer, correct_answer, time_limit));
			}
		} catch (SQLException e) {
			printSQLException(e);
		}
		return questionModels;
	}
	
	public boolean updateQuestion(QuestionModel question) throws SQLException {
		boolean rowUpdated;
		try (Connection connection = getConnection();
				PreparedStatement statement = connection.prepareStatement(UPDATE_QUESTIONS_SQL);) {
			statement.setInt(1, question.getQuizId());
			statement.setString(2, question.getQuestion());
			statement.setString(3, question.getAnswer());
			statement.setString(4, question.getCorrectAnswer());
			statement.setInt(5, question.getTimeLimit());			
			statement.setInt(6, question.getId());
			
			System.out.println(statement);			
			rowUpdated = statement.executeUpdate() > 0;
		}
		return rowUpdated;
	}

	public boolean deleteQuestion(int id) throws SQLException {
		boolean rowDeleted;
		try (Connection connection = getConnection();
				PreparedStatement statement = connection.prepareStatement(DELETE_QUESTIONS_SQL);) {
			statement.setInt(1, id);
			rowDeleted = statement.executeUpdate() > 0;
		}
		return rowDeleted;
	}
	
	private void printSQLException(SQLException ex) {
		for (Throwable e : ex) {
			if (e instanceof SQLException) {
				e.printStackTrace(System.err);
				System.err.println("SQLState: " + ((SQLException) e).getSQLState());
				System.err.println("Error Code: " + ((SQLException) e).getErrorCode());
				System.err.println("Message: " + e.getMessage());
				Throwable t = ex.getCause();
				while (t != null) {
					System.out.println("Cause: " + t);
					t = t.getCause();
				}
			}
		}
	}

}

