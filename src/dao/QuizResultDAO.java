package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.QuizResultModel;

public class QuizResultDAO {
	private String jdbcURL = "jdbc:mysql://localhost:3306/quiz?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    private String jdbcUsername = "root";
    private String jdbcPassword = "";

	private static final String INSERT_QUIZS_SQL 	= "INSERT INTO quiz_results" + "  (user_name, user_email, quiz_id, correct_count, answer_count) VALUES " + " (?, ?, ?, ?, ?);";
	private static final String SELECT_QUIZ_BY_ID 	= "SELECT * FROM quiz_results WHERE id = ?";
	private static final String SELECT_ALL_QUIZS 	= "SELECT quiz_results.*, quizs.title as quiz_title " +
													  "FROM quiz_results LEFT JOIN quizs ON quiz_results.quiz_id = quizs.id";
	private static final String DELETE_QUIZS_SQL 	= "DELETE FROM quiz_results WHERE id = ?;";
	private static final String UPDATE_QUIZS_SQL 	= "UPDATE quiz_results SET user_name = ?, user_email = ?, quiz_id = ?, correct_count = ?, answer_count = ? WHERE id = ?;";

	public QuizResultDAO() {
		
	}

	protected Connection getConnection() {
		Connection connection = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			connection = DriverManager.getConnection(jdbcURL, jdbcUsername, jdbcPassword);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return connection;
	}

	public boolean createQuizResult(QuizResultModel quizResult) throws SQLException {
		boolean rowInserted = false;
		try (Connection connection = getConnection();
				PreparedStatement statement = connection.prepareStatement(INSERT_QUIZS_SQL)) {
			statement.setString(1, quizResult.getUserName());
			statement.setString(2, quizResult.getUserEmail());
			statement.setInt(3, quizResult.getQuizId());
			statement.setString(4, quizResult.getCorrectCount());
			statement.setString(5, quizResult.getAnswerCount());
			
			System.out.println(statement);
			rowInserted = statement.executeUpdate() > 0;
			
		} catch (SQLException e) {
			printSQLException(e);
		}
		return rowInserted;
	}

	public QuizResultModel selectQuizResult(int id) {
		QuizResultModel quizResult = null;
		try (Connection connection = getConnection();
				PreparedStatement statement = connection.prepareStatement(SELECT_QUIZ_BY_ID);) {
			statement.setInt(1, id);
			
			System.out.println(statement);
			ResultSet rs = statement.executeQuery();

			while (rs.next()) {
				String user_name = rs.getString("user_name");
				String user_email = rs.getString("user_email");
				int quiz_id = Integer.parseInt(rs.getString("quiz_id"));
				String correct_count = rs.getString("correct_count");
				String answer_count = rs.getString("answer_count");
				quizResult = new QuizResultModel(id, user_name, user_email, quiz_id, correct_count, answer_count);
			}
		} catch (SQLException e) {
			printSQLException(e);
		}
		return quizResult;
	}
	
	public List<QuizResultModel> selectAllQuizResults() {

		List<QuizResultModel> quiz_results = new ArrayList<>();
		try (Connection connection = getConnection();
			PreparedStatement statement = connection.prepareStatement(SELECT_ALL_QUIZS);) {
			
			ResultSet rs = statement.executeQuery();
			while (rs.next()) {
				int id = rs.getInt("id");
				String user_name = rs.getString("user_name");
				String user_email = rs.getString("user_email");
				int quiz_id = Integer.parseInt(rs.getString("quiz_id"));
				String quiz_title = rs.getString("quiz_title");
				String correct_count = rs.getString("correct_count");
				String answer_count = rs.getString("answer_count");
				String create_at = rs.getString("create_at");
				quiz_results.add(new QuizResultModel(id, user_name, user_email, quiz_id, quiz_title, correct_count, answer_count, create_at));
			}
		} catch (SQLException e) {
			printSQLException(e);
		}
		return quiz_results;
	}

	public boolean updateQuizResult(QuizResultModel quizResult) throws SQLException {
		boolean rowUpdated;
		try (Connection connection = getConnection();
				PreparedStatement statement = connection.prepareStatement(UPDATE_QUIZS_SQL);) {
			statement.setString(1, quizResult.getUserName());
			statement.setString(2, quizResult.getUserEmail());
			statement.setInt(3, quizResult.getQuizId());
			statement.setString(4, quizResult.getCorrectCount());
			statement.setString(5, quizResult.getAnswerCount());
			
			statement.setInt(6, quizResult.getId());
			
			System.out.println(statement);			
			rowUpdated = statement.executeUpdate() > 0;
		}
		return rowUpdated;
	}

	public boolean deleteQuizResult(int id) throws SQLException {
		boolean rowDeleted;
		try (Connection connection = getConnection();
				PreparedStatement statement = connection.prepareStatement(DELETE_QUIZS_SQL);) {
			statement.setInt(1, id);
			rowDeleted = statement.executeUpdate() > 0;
		}
		return rowDeleted;
	}
	
	private void printSQLException(SQLException ex) {
		for (Throwable e : ex) {
			if (e instanceof SQLException) {
				e.printStackTrace(System.err);
				System.err.println("SQLState: " + ((SQLException) e).getSQLState());
				System.err.println("Error Code: " + ((SQLException) e).getErrorCode());
				System.err.println("Message: " + e.getMessage());
				Throwable t = ex.getCause();
				while (t != null) {
					System.out.println("Cause: " + t);
					t = t.getCause();
				}
			}
		}
	}

}

