package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import model.QuizModel;

public class QuizDAO {
	private String jdbcURL = "jdbc:mysql://localhost:3306/quiz?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    private String jdbcUsername = "root";
    private String jdbcPassword = "";

	private static final String INSERT_QUIZS_SQL 	  = "INSERT INTO quizs" + "  (user_id, title, image) VALUES " + " (?, ?, ?);";
	private static final String SELECT_QUIZ_BY_ID 	  = "SELECT * FROM quizs WHERE id = ?";
	private static final String SELECT_QUIZ_BY_RANDOM = "SELECT * FROM quizs ORDER BY RAND() LIMIT 1";
	private static final String SELECT_ALL_QUIZS 	  = "SELECT * FROM quizs";
	private static final String DELETE_QUIZS_SQL 	  = "DELETE FROM quizs WHERE id = ?;";
	private static final String UPDATE_QUIZS_SQL 	  = "UPDATE quizs SET user_id = ?, title = ?, image = ? WHERE id = ?;";

	public QuizDAO() {
		
	}

	protected Connection getConnection() {
		Connection connection = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			connection = DriverManager.getConnection(jdbcURL, jdbcUsername, jdbcPassword);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return connection;
	}
	
	public int getOnlineUsers(String session_id) {
		int count = 0;
		
		try (Connection connection = getConnection()) {
			
			Statement statement = null;	       
        	statement = connection.createStatement();
        	String sql = "SELECT COUNT(*) FROM useronline WHERE session='" + session_id +"'";
        	
            ResultSet rs = statement.executeQuery(sql);	            
            
            count = 0;
            while (rs.next()) {
            	count = rs.getInt(1);
			}
            
            Date now = new Date();
            String pattern = "yyyy-MM-dd";
            SimpleDateFormat formatter = new SimpleDateFormat(pattern);
            String date = formatter.format(now);
            
			if(count == 0) {
				sql = "INSERT INTO useronline(session, time) VALUES('" + session_id + "', '" + date + "')";
				System.out.println(sql);
				statement.executeUpdate(sql);
			} else {
				sql = "UPDATE useronline SET time = '" + date + "' WHERE session='" + session_id + "'"; 
				statement.executeUpdate(sql);
			}
			
        	sql = "SELECT COUNT(*) FROM useronline";
            rs = statement.executeQuery(sql);
            
            count = 0;
            while (rs.next()) {
				count = rs.getInt(1);
			}
            
		} catch (SQLException e) {
			printSQLException(e);
		}
		return count;
	}
	
	public boolean createQuiz(QuizModel quiz) throws SQLException {
		boolean rowInserted = false;
		try (Connection connection = getConnection();
				PreparedStatement statement = connection.prepareStatement(INSERT_QUIZS_SQL)) {
			statement.setInt(1, quiz.getUserId());
			statement.setString(2, quiz.getTitle());
			statement.setString(3, quiz.getImage());
			
			System.out.println(statement);
			rowInserted = statement.executeUpdate() > 0;
			
		} catch (SQLException e) {
			printSQLException(e);
		}
		return rowInserted;
	}

	public QuizModel selectQuiz(int id) {
		QuizModel quiz = null;
		try (Connection connection = getConnection();
				PreparedStatement statement = connection.prepareStatement(SELECT_QUIZ_BY_ID);) {
			statement.setInt(1, id);
			
			System.out.println(statement);
			ResultSet rs = statement.executeQuery();

			while (rs.next()) {
				int user_id = rs.getInt("user_id");
				String title = rs.getString("title");
				String image = rs.getString("image");
				quiz = new QuizModel(id, user_id, title, image);
			}
		} catch (SQLException e) {
			printSQLException(e);
		}
		return quiz;
	}
	
	public QuizModel selectRandomQuiz() {
		QuizModel quiz = null;
		try (Connection connection = getConnection();
				PreparedStatement statement = connection.prepareStatement(SELECT_QUIZ_BY_RANDOM);) {
			
			System.out.println(statement);
			ResultSet rs = statement.executeQuery();

			while (rs.next()) {
				int id = rs.getInt("id");
				int user_id = rs.getInt("user_id");
				String title = rs.getString("title");
				String image = rs.getString("image");
				quiz = new QuizModel(id, user_id, title, image);
			}
		} catch (SQLException e) {
			printSQLException(e);
		}
		return quiz;
	}
	
	public List<QuizModel> selectAllQuizs() {

		List<QuizModel> quizs = new ArrayList<>();
		try (Connection connection = getConnection();
			PreparedStatement statement = connection.prepareStatement(SELECT_ALL_QUIZS);) {
			
			ResultSet rs = statement.executeQuery();
			while (rs.next()) {
				int id = rs.getInt("id");
				int user_id = rs.getInt("user_id");
				String title = rs.getString("title");
				String image = rs.getString("image");
				quizs.add(new QuizModel(id, user_id, title, image));
			}
		} catch (SQLException e) {
			printSQLException(e);
		}
		return quizs;
	}

	public boolean updateQuiz(QuizModel quiz) throws SQLException {
		boolean rowUpdated;
		try (Connection connection = getConnection();
				PreparedStatement statement = connection.prepareStatement(UPDATE_QUIZS_SQL);) {
			statement.setInt(1, quiz.getUserId());
			statement.setString(2, quiz.getTitle());
			statement.setString(3, quiz.getImage());			
			statement.setInt(4, quiz.getId());
			
			System.out.println(statement);			
			rowUpdated = statement.executeUpdate() > 0;
		}
		return rowUpdated;
	}

	public boolean deleteQuiz(int id) throws SQLException {
		boolean rowDeleted;
		try (Connection connection = getConnection();
				PreparedStatement statement = connection.prepareStatement(DELETE_QUIZS_SQL);) {
			statement.setInt(1, id);
			rowDeleted = statement.executeUpdate() > 0;
		}
		return rowDeleted;
	}
	
	private void printSQLException(SQLException ex) {
		for (Throwable e : ex) {
			if (e instanceof SQLException) {
				e.printStackTrace(System.err);
				System.err.println("SQLState: " + ((SQLException) e).getSQLState());
				System.err.println("Error Code: " + ((SQLException) e).getErrorCode());
				System.err.println("Message: " + e.getMessage());
				Throwable t = ex.getCause();
				while (t != null) {
					System.out.println("Cause: " + t);
					t = t.getCause();
				}
			}
		}
	}

}

