<jsp:include page="layouts/header.jsp" />

<div class="container mt-5" >
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Sign up</div>
                <div class="card-body">
                    <form id = "signup-form" action="signup" method="post">
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Name</label>
                            <div class="col-md-6">
                                <input type="text" id="name" class="form-control" name="name" required autofocus>
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">E-Mail Address</label>
                            <div class="col-md-6">
                                <input type="text" id="email_address" class="form-control" name="email" required autofocus>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">Password</label>
                            <div class="col-md-6">
                                <input type="password" id="password" class="form-control" name="password" minlength="6" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="confirm_password" class="col-md-4 col-form-label text-md-right">Confirm Password</label>
                            <div class="col-md-6">
                                <input type="password" id="confirm_password" class="form-control" name="confirm_password" minlength="6" required>
                                <div class="invalid-feedback"></div>
                            </div>
                        </div>

                        <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn btn-primary signup-submit">
                                Sign up
                            </button>
                        </div>                
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Scripts -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.5/validator.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

<script src="https://use.fontawesome.com/releases/v5.8.1/js/solid.js" ></script>
<script src="https://use.fontawesome.com/releases/v5.8.1/js/fontawesome.js"></script>

<script src="a.js"></script>

<script>

$( document ).ready(function() {

	/* Submit Signup*/
	$("#signup-form").submit(function(e) {

	    e.preventDefault();
	    var form_action = $(this).attr("action");
	    var name = $(this).find("input[name='name']").val();
		var email = $(this).find("input[name='email']").val();
		var password = $(this).find("input[name='password']").val();
		var confirm_password = $(this).find("input[name='confirm_password']").val();

		if(password != confirm_password) {
			$(".invalid-feedback").text('Passwords do not match.');
			$(".invalid-feedback").css('display', 'block'); return;
		}
		
		$.ajax({
			dataType: 'json',
			type:'POST',
			url: form_action,
			data:{name:name, email:email, password:password}
		}).done(function(data){
				
			if(data == true) {
				location.href = "index.jsp";
			} else {
				$(".invalid-feedback").text('Invalid E-Mmail Address.');
				$(".invalid-feedback").css('display', 'block');
			}
			
		});

	});
	   
});

</script>
</body>

</html>
