<jsp:include page="layouts/header.jsp" />

<div class="container mt-5">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Login</div>
                <div class="card-body">
                    <form id = "login-form" action="login" method="post">
                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">E-Mail Address</label>
                            <div class="col-md-6">
                                <input type="text" id="email" class="form-control" name="email" required autofocus>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">Password</label>
                            <div class="col-md-6">
                                <input type="password" id="password" class="form-control" name="password" minlength="6" required>
                                <div class="invalid-feedback">Invalid email or password</div>
                            </div>
                        </div>         				
                        
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn btn-primary login-submit">
                                Login
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Scripts -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.5/validator.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

<script src="https://use.fontawesome.com/releases/v5.8.1/js/solid.js" ></script>
<script src="https://use.fontawesome.com/releases/v5.8.1/js/fontawesome.js"></script>

<script>

$( document ).ready(function() {

	/* Submit login Form*/
	$("#login-form").submit(function(e) {

		e.preventDefault();
	    var form_action = $(this).attr("action");
	    var email = $(this).find("input[name='email']");
		var password = $(this).find("input[name='password']");
		
		$.ajax({
			dataType: 'json',
			type:'POST',
			url: form_action,
			data:{email:email.val(), password:password.val()}
		}).done(function(data){
			if(data == true) {
				location.href = "index.jsp";
			} else {
				$(".invalid-feedback").css('display', 'block');
			}
		});

	});
	
});

</script>
</body>

</html>
