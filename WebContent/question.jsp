<jsp:include page="layouts/header.jsp" />

<% if (session.getAttribute("email") == null) { 
	response.sendRedirect("login.jsp");
} %>

<div class="container mt-3">
	<div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
				<div class="card-header">
                    Question Manage
                    <button class="btn btn-default float-right border go-quiz">
						<i class="fas fa-arrow-circle-left"></i> BACK QUIZ
					</button>
                </div>
				<div class="card-body">
					<input type = "hidden" class = "quiz-id" value = <% out.print(request.getParameter("quiz_id")); %> />
					<div class="p-0 mb-2">
						<button class="btn btn-success" data-toggle="modal" data-target="#create-item">
							<i class="fas fa-plus mr-2"></i>NEW QUESTION
						</button>
					</div>
					<table id = "question" class="table text-center">
						<thead>
							<tr>
								<th style="width:10%;">No</th>
								<th style="width:50%;">Question</th>
								<th style="width:20%;">Time Limit</th>
								<th style="width:20%;">Action</th>								
							</tr>
						</thead>
						<tbody>
						</tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
	
</div>

<!-- Create Item Modal -->
<div class="modal" id="create-item">
	<div class="modal-dialog">
		<div class="modal-content">

			<!-- Modal Header -->
			<div class="modal-header">
				<h4 class="modal-title">Create A New Question</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>

			<!-- Modal body -->
			<div class="modal-body">
				<form action="question/create" enctype="multipart/form-data" method="post">
					<div class="form-group">
						<label for="question">Question:</label>
						<input type="text" class="form-control" placeholder="Enter qustion" name="question"  required>
					</div>
					
					<div class="form-group">
						<label for="answerCount">Choose answer count:</label>
						<select class="form-control" name="answerCount" id="answerCount">
							<option value="2">2</option>
							<option value="3" selected>3</option>
							<option value="4">4</option>
							<option value="5">5</option>
							<option value="6">6</option>
						</select>
					</div>
					
					<div id="answerGroup">
					<div class="form-group">
						<label for="answer1">Answer 1:</label>
						<input type="text" class="form-control" placeholder="Enter answer" name="answer1"  required>
						<input type="checkbox" class="form-control mt-2" name="correct_answer1">
					</div>
					
					<div class="form-group">
						<label for="answer2">Answer 2:</label>
						<input type="text" class="form-control" placeholder="Enter answer" name="answer2"  required>
						<input type="checkbox" class="form-control mt-2" name="correct_answer2">
					</div>
					
					<div class="form-group">
						<label for="answer3">Answer 3:</label>
						<input type="text" class="form-control" placeholder="Enter answer" name="answer3"  required>
						<input type="checkbox" class="form-control mt-2" name="correct_answer3">
					</div>
					</div>
					
					<div class="form-group">
						<label for="time_limit">Time Limit:</label>
						<input type="text" class="form-control" placeholder="Enter time limit(second)" name="time_limit" value = 60 required>
					</div>
					
					<div class="form-group">
						<button type="submit" class="btn btn-primary create-submit">Submit</button>
					</div>
					
				</form>
			</div>

			<!-- Modal footer -->
			<div class="modal-footer">
				
			</div>

		</div>
	</div>
</div>

<!-- Edit Item Modal -->
<div class="modal" id="edit-item">
	<div class="modal-dialog">
		<div class="modal-content">

			<!-- Modal Header -->
			<div class="modal-header">
				<h4 class="modal-title">Edit A Question</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>

			<!-- Modal body -->
			<div class="modal-body">

				<form action="question/update">
					<input type = "hidden" class = "edit-id" />
					<div class="form-group">
						<label for="question">Question:</label>
						<input type="text" class="form-control" placeholder="Enter qustion" name="question" required>
					</div>
					
					<div class="form-group">
						<label for="answerCountEdit">Choose answer count:</label>
						<select class="form-control" name="answerCountEdit" id="answerCountEdit">
							<option value="2">2</option>
							<option value="3" selected>3</option>
							<option value="4">4</option>
							<option value="5">5</option>
							<option value="6">6</option>
						</select>
					</div>
					
					<div id="answerGroupEdit">
					</div>
					
					<div class="form-group">
						<label for="time_limit">Time Limit:</label>
						<input type="text" class="form-control" placeholder="Enter time limit" name="time_limit" value = 60 required>
					</div>
					
					<div class="form-group">
						<button type="submit" class="btn btn-primary edit-submit">Submit</button>
					</div>
					
				</form>
			</div>

			<!-- Modal footer -->
			<div class="modal-footer">				
			</div>

		</div>
	</div>
</div>
<!-- Scripts -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.5/validator.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

<script src="https://use.fontawesome.com/releases/v5.8.1/js/solid.js" ></script>
<script src="https://use.fontawesome.com/releases/v5.8.1/js/fontawesome.js"></script>

<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.9/validator.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>

<!-- <script src="assets/js/question.js"></script>  -->

<script>

$( document ).ready(function() {
	
    var altTimeOut = 2000;
    var answerCount = 3;
    var answerCountEdit = 3;
    
	getQuestions();

	function getQuestions() {
		var quiz_id = $(".quiz-id").val();
		$.ajax({
			type:'POST',
	    	url: 'question/list',
	    	dataType: 'json',
	    	data:{quiz_id:quiz_id}
		}).done(function(data){
			manageRow(data);
	    	$('#question').DataTable();
		});
	}

	function manageRow(data) {
		var	rows = '';
        var cnt = 0;
        
		$.each( data, function( key, value ) {
	    	cnt++;
			var row = `
	        	<tr data-id = "` + value.id + `" 
	        		data-answer = '` + value.answer + `' 
	        		data-correct_answer = '` + value.correct_answer + `'>
	        	<td>` + cnt + `</td>
	            <td>` + value.question + `</td>
	            <td>` + value.time_limit + `</td>
	            <td>
		            <button class = "btn btn-info mr-2 edit-item" data-toggle="modal" data-target="#edit-item"><i class='fas fa-pen-alt'></i></button>
	            	<button class = "btn btn-danger delete-item"><i class='fas fa-trash-alt'></i></button>
	            </td>
            </tr>`;
	    	rows = rows + row;
		});

		$("tbody").html(rows);
	}
	

	/* Create new Item */
	$("#create-item").find("form").submit(function(e) {
	    e.preventDefault();
	    var form_action = $(this).attr("action");
	    var quiz_id = $(".quiz-id").val();
		var question = $(this).find("input[name='question']").val();
		
		var arrAnswer = [];
		var arrCorrectAnswer = [];
		for(var i = 0; i < answerCount; i++) {
			arrAnswer[i] = $(this).find("input[name='answer" + (i + 1) + "']").val();
			arrCorrectAnswer[i] = $(this).find("input[name='correct_answer" + (i + 1) + "']").prop('checked');
	    }
		var answer = JSON.stringify(arrAnswer);
		var correct_answer = JSON.stringify(arrCorrectAnswer);
		
		var time_limit = $(this).find("input[name='time_limit']").val();
	    	
		$.ajax({
			dataType: 'json',
			type: 'POST',
			url: form_action,
			data: {quiz_id:quiz_id, question:question, answer:answer, correct_answer:correct_answer, time_limit:time_limit}
		}).done(function(data){
			if (data == true) {
				$(this).find("input[name='question']").val('');
				for(var i = 0; i < answerCount; i++) {
					$(this).find("input[name='answer" + (i + 1) + "']").val('');
					$(this).find("input[name='correct_answer" + (i + 1) + "']").prop('checked', false);
			    }
				$(this).find("input[name='time_limit']").val(60);
				
				getQuestions();
				$(".modal").modal('hide');
				toastr.success('Created Successfully.', 'Success Alert', {timeOut: altTimeOut});
			} else {
				toastr.error('Failed to create.', 'Failed Alert', {timeOut: altTimeOut});
			}
		});

	});
	
	$("#answerCount").change(function(e) {
		var rows="", row;
		answerCount = $(this).val();
		for(var i = 1; i <= answerCount; i++) {		
			row = `
			<div class="form-group">
				<label for="answer` + i + `">Answer ` + i + `:</label>
				<input type="text" class="form-control" placeholder="Enter answer" name="answer` + i + `" required>
				<input type="checkbox" class="form-control mt-2" name="correct_answer` + i + `">
			</div>`;
			rows += row;
		}
		$("#answerGroup").html(rows);
	
	});
	
	/* Edit Item */
	$("body").on("click",".edit-item",function(){

	    var tr = $(this).closest("tr");
	    
	    var id = tr.data('id');	    
	    var answer = tr.data('answer');
	    var correct_answer = tr.data('correct_answer');
	     
	    var question = tr.find("td:eq(1)").text();	    
	    var time_limit = tr.find("td:eq(2)").text();	    
    	
    	$("#edit-item").find("input[name='question']").val(question);
    	
    	var rows = '';
    	answerCountEdit = answer.length;
    	$("#answerCountEdit").val(answerCountEdit);
		for(var i = 0; i < answerCountEdit; i++) {		
			row = `
			<div class="form-group">
				<label for="answer` + i + `">Answer ` + (i + 1) + `:</label>
				<input type="text" class="form-control" placeholder="Enter answer" name="answer` + (i + 1) + `" required>
				<input type="checkbox" class="form-control mt-2" name="correct_answer` + (i + 1) + `">
			</div>`;
			rows += row;
		}
		$("#answerGroupEdit").html(rows);
		 
    	var arrAnswer = [];
		var arrCorrectAnswer = [];
		for(var i = 0; i < answerCountEdit; i++) {
			$("#edit-item").find("input[name='answer" + (i + 1) + "']").val(answer[i]);
			$("#edit-item").find("input[name='correct_answer" + (i + 1) + "']").prop('checked', correct_answer[i]);
		}
		
		$("#edit-item").find("input[name='time_limit']").val(time_limit);
	    $("#edit-item").find(".edit-id").val(id);

	});
	
	$("#answerCountEdit").change(function(e) {
		var rows="", row;
		answerCountEdit = $(this).val();
		for(var i = 1; i <= answerCountEdit; i++) {		
			row = `
			<div class="form-group">
				<label for="answer` + i + `">Answer ` + i + `:</label>
				<input type="text" class="form-control" placeholder="Enter answer" name="answer` + i + `" required>
				<input type="checkbox" class="form-control mt-2" name="correct_answer` + i + `">
			</div>`;
			rows += row;
		}
		$("#answerGroupEdit").html(rows);
	
	});
	
	/* Update new Item */
	$("#edit-item").find("form").submit(function(e) {

		e.preventDefault();
		var form_action = $(this).attr("action");
	    var id = $("#edit-item").find(".edit-id").val();
	    var quiz_id = $(".quiz-id").val();
		var question = $(this).find("input[name='question']").val();
		
		var arrAnswer = [];
		var arrCorrectAnswer = [];
		for(var i = 0; i < answerCountEdit; i++) {
			arrAnswer[i] = $(this).find("input[name='answer" + (i + 1) + "']").val();
			arrCorrectAnswer[i] = $(this).find("input[name='correct_answer" + (i + 1) + "']").prop('checked');
	    }
		
	    var answer = JSON.stringify(arrAnswer);
		var correct_answer = JSON.stringify(arrCorrectAnswer);
		
		var time_limit = $(this).find("input[name='time_limit']").val();
	    	
		$.ajax({
			dataType: 'json',
			type: 'POST',
			url: form_action,
			data: {id:id, quiz_id:quiz_id, question:question, answer:answer, correct_answer:correct_answer, time_limit:time_limit}
		}).done(function(data){
			if (data == true) {
				$(this).find("input[name='question']").val('');
				$(this).find("input[name='time_limit']").val(60);
				
				getQuestions();
				$(".modal").modal('hide');
				toastr.success('Updated Successfully.', 'Success Alert', {timeOut: altTimeOut});
			} else {
				toastr.error('Failed to update.', 'Failed Alert', {timeOut: altTimeOut});
			}
		});

	});
	
	$("body").on("click",".delete-item",function(){
		var id = $(this).closest("tr").data("id");
		
		$.ajax({
			dataType: 'json',
			type:'POST',
	    	url: 'question/delete',
	    	data:{id:id}
		}).done(function(data){
			if (data == true) {
				getQuestions();
				toastr.success('Deleted Successfully.', 'Success Alert', {timeOut: altTimeOut});
			} else {
				toastr.error('Failed to delete.', 'Failed Alert', {timeOut: altTimeOut});
			}
		});
	});
	
	$("body").on("click",".go-quiz",function(){
		location.href = "quiz.jsp";
	});

});

</script>

</body>

</html>
