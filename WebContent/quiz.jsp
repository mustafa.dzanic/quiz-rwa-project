<jsp:include page="layouts/header.jsp" />

<% if (session.getAttribute("email") == null) { 
	response.sendRedirect("login.jsp");
} %>

<div class="container mt-3">
	<div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
				<div class="card-header">
                    Quiz Manage
                </div>
				<div class="card-body">
					<div class="p-0 mb-2">
						<button class="btn btn-success" data-toggle="modal" data-target="#create-item">
							<i class="fas fa-plus mr-2"></i>NEW QUIZ
						</button>
					</div>
					<table id = "quiz" class="table text-center">
						<thead>
							<tr>
								<th style="width:10%;">No</th>
								<th style="width:10%;">Image</th>
								<th style="width:60%;">Title</th>
								<th style="width:20%;">Action</th>								
							</tr>
						</thead>
						<tbody>
						</tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
	
</div>

<!-- Create Item Modal -->
<div class="modal" id="create-item">
	<div class="modal-dialog">
		<div class="modal-content">

			<!-- Modal Header -->
			<div class="modal-header">
				<h4 class="modal-title">Create A New Quiz</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>

			<!-- Modal body -->
			<div class="modal-body">
				<form action="quiz/create" enctype="multipart/form-data" method="post">
					<input type = "hidden" class = "edit-user-id" value = <% out.print(session.getAttribute("user_id")); %> />
					
					<div class="form-group">
						<label for="title">Title:</label>
						<input type="text" class="form-control" placeholder="Enter qustion" name="title"  required>
					</div>
					
					<div class="form-group">
						Image: <input type="file" id="imageServlet" name="imageServlet" required/>
					</div>
					
					<div class="form-group">
						<button type="submit" class="btn btn-primary create-submit">Submit</button>
					</div>
					
				</form>
			</div>

			<!-- Modal footer -->
			<div class="modal-footer">
				
			</div>

		</div>
	</div>
</div>

<!-- Edit Item Modal -->
<div class="modal" id="edit-item">
	<div class="modal-dialog">
		<div class="modal-content">

			<!-- Modal Header -->
			<div class="modal-header">
				<h4 class="modal-title">Edit A Quiz</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>

			<!-- Modal body -->
			<div class="modal-body">

				<form action="quiz/update">
					<input type = "hidden" class = "edit-id" />
					<input type = "hidden" class = "edit-user-id" value = <% out.print(session.getAttribute("user_id")); %> />
					<div class="form-group">
						<label for="title">Title:</label>
						<input type="text" class="form-control" placeholder="Enter title" name="title" required>
					</div>
					
					<div class="form-group">
						Image: <input type="file" id="imageServlet" name="imageServlet" required/>
					</div>
					
					<div class="form-group">
						<button type="submit" class="btn btn-primary edit-submit">Submit</button>
					</div>
					
				</form>
			</div>

			<!-- Modal footer -->
			<div class="modal-footer">				
			</div>

		</div>
	</div>
</div>
<!-- Scripts -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.5/validator.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

<script src="https://use.fontawesome.com/releases/v5.8.1/js/solid.js" ></script>
<script src="https://use.fontawesome.com/releases/v5.8.1/js/fontawesome.js"></script>

<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.9/validator.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>

<!-- <script src="assets/js/quiz.js"></script>  -->

<script>

$( document ).ready(function() {
	
    var altTimeOut = 2000;
    var answerCount = 3;
    var answerCountEdit = 3;
    
	getQuizs();

	function getQuizs() {
		$.ajax({
			type:'POST',
	    	url: 'quiz/list'
		}).done(function(data){
			manageRow(data);
	    	$('#quiz').DataTable();
		});
	}

	function manageRow(data) {
		var	rows = '';
        var cnt = 0;
        
		$.each( data, function( key, value ) {
	    	cnt++;
			var row = `<tr data-id = "` + value.id + `">
	        	<td>` + cnt + `</td>
	        	<td><img style="width:100%;height:80px;" src='` + "uploads/" + value.image + `'></td>
	            <td><a href='question.jsp?quiz_id=` + value.id + `'>` + value.title + `</a></td>
	            <td>
		            <button class = "btn btn-info mr-2 edit-item" data-toggle="modal" data-target="#edit-item"><i class='fas fa-pen-alt'></i></button>
	            	<button class = "btn btn-danger delete-item"><i class='fas fa-trash-alt'></i></button>
	            </td>
            </tr>`;
	    	rows = rows + row;
		});

		$("tbody").html(rows);
	}
	

	/* Create new Item */
	$("#create-item").find("form").submit(function(e) {
	    e.preventDefault();
	    var form_action = $(this).attr("action");
	    var user_id = $("#create-item").find(".edit-user-id").val();
		var title = $(this).find("input[name='title']").val();
		
		var file_data = $(this).find('#imageServlet').prop('files')[0];
		var form_data = new FormData();                  
	    form_data.append('file', file_data);
		
		var obj = {user_id:user_id, title:title};
		form_data.append('obj', JSON.stringify(obj));
		
		$.ajax({
			type:'POST',
			url: form_action,
			cache: false,
	        contentType: false,
	        processData: false,
	        data: form_data
		}).done(function(data){
			if (data == true) {
				$(this).find("input[name='title']").val('');
				
				getQuizs();
				$(".modal").modal('hide');
				toastr.success('Created Successfully.', 'Success Alert', {timeOut: altTimeOut});
			} else {
				toastr.error('Failed to create.', 'Failed Alert', {timeOut: altTimeOut});
			}
		});

	});
	
	/* Edit Item */
	$("body").on("click",".edit-item",function(){

	    var tr = $(this).closest("tr");
	    
	    var id = tr.data('id');
	    var user_id = $("#edit-item").find(".edit-user-id").val();
		var title = tr.find("td:eq(2)").text();	    
	    
    	$("#edit-item").find("input[name='title']").val(title);
    	
    	$("#edit-item").find(".edit-id").val(id);

	});
	
	/* Update new Item */
	$("#edit-item").find("form").submit(function(e) {

		e.preventDefault();
		var form_action = $(this).attr("action");
	    var id = $("#edit-item").find(".edit-id").val();
		var user_id = $("#edit-item").find(".edit-user-id").val();
		var title = $(this).find("input[name='title']").val();
		
		var file_data = $(this).find('#imageServlet').prop('files')[0];
		var form_data = new FormData();                  
	    form_data.append('file', file_data);
		
		var obj = {id:id, user_id:user_id, title:title};
		form_data.append('obj', JSON.stringify(obj));
		
		$.ajax({
			type:'POST',
			url: form_action,
			cache: false,
	        contentType: false,
	        processData: false,
	        data: form_data
		}).done(function(data){
			if (data == true) {
				$(this).find("input[name='title']").val('');
				
				getQuizs();
				$(".modal").modal('hide');
				toastr.success('Updated Successfully.', 'Success Alert', {timeOut: altTimeOut});
			} else {
				toastr.error('Failed to update.', 'Failed Alert', {timeOut: altTimeOut});
			}
		});

	});
	
	$("body").on("click",".delete-item",function(){
		var id = $(this).closest("tr").data("id");
		
		$.ajax({
			dataType: 'json',
			type:'POST',
	    	url: 'quiz/delete',
	    	data:{id:id}
		}).done(function(data){
			if (data == true) {
				getQuizs();
				toastr.success('Deleted Successfully.', 'Success Alert', {timeOut: altTimeOut});
			} else {
				toastr.error('Failed to delete.', 'Failed Alert', {timeOut: altTimeOut});
			}
		});
	});

});

</script>

</body>

</html>
