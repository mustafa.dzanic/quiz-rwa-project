
<jsp:include page="layouts/header.jsp" />

<div class="container mt-3">
	<div class="quiz1 row justify-content-center">
        <div class="col-md-12">
            <div class="card">
				<div class="card-header">
                    <a class="align-middle">Quiz1</a>
                    <button class="btn btn-info float-right d-none quiz-next" title="Next"><i class="fas fa-arrow-right"></i> Next</button>
				</div>
				<div class="card-body">
					<div class="startStage d-flex justify-content-center">
                        <form class="col-md-3 text-center">
	                        <div class="form-group">
								<label for="questions_number">Enter number of questions: </label>
								<input type="number" class="questions_number form-control" name="questions_number" min=1 max=25 value=2 required></input>
                            </div>
                            <div class="form-group">
	                            <button type="submit" class="btn btn-default border play-submit" style="width:100px;">
	                            	<i class="fas fa-play"></i>&nbsp;&nbsp;Play
	                            </button>
                            </div>
                        </form>
	                </div>
	                <div class="quizStage d-none p-1">
	                    <div class="quizHeader mt-2">	                    
	                    </div>
	                    <div class="quizBody mt-2">
	                    </div>
	                </div>
	                <div class="submitStage justify-content-center d-none p-3">
	                    <form class="col-md-6" action="quizresult/create">
							<div class="form-group">
								<label for="name">Name:</label>
								<input type="text" class="form-control" placeholder="Enter name" name="name" required>
							</div>							
							<div class="form-group">
								<label for="email">E-Mail Address:</label>
								<input type="text" class="form-control" placeholder="Enter email address" name="email" required>
							</div>						
							<div class="form-group">
								<button type="submit" class="btn btn-primary quiz-submit">Submit</button>
							</div>							
						</form>
	                </div>
	                <div class="feedbackStage text-center d-none p-3">
	                    <h4 class="feedback"></h4>
	                    <div class="answer"></div>
	                    <div class="percent"></div>
	                </div>
                </div>
            </div>
        </div>
    </div>
    <div class="quiz2 mt-3 row justify-content-center">
        <div class="col-md-12">
            <div class="card">
				<div class="card-header">
                    <a class="align-middle">Quiz2</a>
                    <button class="btn btn-info float-right d-none quiz-next" title="Next"><i class="fas fa-arrow-right"></i> Next</button>
				</div>
				<div class="card-body">
					<div class="startStage d-flex justify-content-center">
                        <form class="col-md-3 text-center">
	                        <div class="form-group">
								<label for="questions_number">Enter number of questions: </label>
								<input type="number" class="questions_number form-control" name="questions_number" min=1 max=25 value=2 required></input>
                            </div>
                            <div class="form-group">
	                            <button type="submit" class="btn btn-default border play-submit" style="width:100px;">
	                            	<i class="fas fa-play"></i>&nbsp;&nbsp;Play
	                            </button>
                            </div>
                        </form>
	                </div>
	                <div class="quizStage d-none p-1">
	                    <div class="quizHeader mt-2">	                    
	                    </div>
	                    <div class="quizBody mt-2">
	                    </div>
	                </div>
	                <div class="submitStage justify-content-center d-none p-3">
	                    <form class="col-md-6" action="quizresult/create">
							<div class="form-group">
								<label for="name">Name:</label>
								<input type="text" class="form-control" placeholder="Enter name" name="name" required>
							</div>							
							<div class="form-group">
								<label for="email">E-Mail Address:</label>
								<input type="text" class="form-control" placeholder="Enter email address" name="email" required>
							</div>						
							<div class="form-group">
								<button type="submit" class="btn btn-primary quiz-submit">Submit</button>
							</div>							
						</form>
	                </div>
	                <div class="feedbackStage text-center d-none p-3">
	                    <h4 class="feedback"></h4>
	                    <div class="answer"></div>
	                    <div class="percent"></div>
	                </div>
                </div>
            </div>
        </div>
    </div>    
</div>
<footer class="footer">
	<div class="container text-center">
		<span class="player-numbers align-middle"></span>
	</div>
</footer>
<!-- Scripts -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.5/validator.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

<script src="https://use.fontawesome.com/releases/v5.8.1/js/solid.js" ></script>
<script src="https://use.fontawesome.com/releases/v5.8.1/js/fontawesome.js"></script>

<script>

$( document ).ready(function() {
	
	var quiz_id;
	var questionNo = 1;
	var questions_number;
	
	var answerLength;
	var correct_answer;
	
	var correct_count = 0;
	var answer_count = 0;
	
	var quizInterval;
	
	var quiz_id_2;			
	var questionNo_2 = 1;	
	var questions_number_2;	
	
	var answerLength_2;		
	var correct_answer_2;	
	
	var correct_count_2 = 0;
	var answer_count_2 = 0;	
	
	var quizInterval_2;	
	
	/*$.getJSON('https://ipinfo.io/json', function(data) {
	  console.log(JSON.stringify(data, null, 2));
	});*/
	
	$.ajax({
		url : 'quiz/getOnlineUsers',
		type: 'POST',
		dataType: 'json'
	}).done(function(data){
		$(".player-numbers").html("Player Numbers : " + data);
	});
	
	$(".quiz1 .startStage").find("form").submit(function(e) {
		
		e.preventDefault();
		questions_number = $(".quiz1 .questions_number").val();
		
		$.ajax({
			url : 'quiz/getRandom',
			type: 'POST',
			dataType: 'json'
		}).done(function(quiz){
			
			var title = quiz.title;
			var image = "uploads/" + quiz.image;
			quiz_id = quiz.id;
			
			$.ajax({
				url : 'question/getRandom',
				type: 'POST',
				dataType: 'json',
				data: {quiz_id:quiz_id, questions_number:questions_number}
			}).done(function(data){
				
				$(".quiz1 .card-header a").html(title);
				$(".quiz1 .startStage").removeClass("d-flex");
				$(".quiz1 .startStage").addClass("d-none");
				
				var timelimit = 0;
				var	questions = '';
                var cnt = 0;
                
                answerLength = [];
                correct_answer = [];
                
        		$.each( data, function( key, value ) {
        	    	cnt++;
        	    	timelimit += value.time_limit;
        	    	
        	    	var answer = JSON.parse(value.answer);
    				answerLength[cnt] = answer.length;			
    				correct_answer[cnt] = JSON.parse(value.correct_answer);
    				
    				var rows = `<h5>` + value.question + `</h5>
    					<div class="col-md-8">`;
    				for(var i = 0; i < answerLength[cnt]; i++) {            
    					row = `
    						<div class="m-2">
    			            	<input type="checkbox" name="answer` + (i + 1) + `"> ` + answer[i] + `
    			            </div>`;
    			    	rows += row;
    				}    				
    				rows += `</div>`;
    				
        			var question = `
        				<div class="questionGroup` + cnt + `" style="display:none;">` + rows + `
        	        	</div>`;
                    questions += question;
        		});
        		if (cnt < questions_number) questions_number = cnt;
        		
        		var header = `
					<div class="text-center question-bell">
			           	<h1><i class="fas fa-bell"></i><a>` + "&nbsp;00:" + timeFormat(timelimit) + `</a></h1>
			        </div>
			        <div style="text-align:center;width:100px;">
						<img src='` + image + `' style = "width:100%;">
					</div>`;
            	$(".quiz1 .quizHeader").html(header);
            	
            	$('.quiz1 .quizBody').html(questions);
				
				$(".quiz1 .quizStage").removeClass("d-none");
				$(".quiz1 .quizStage").addClass("d-block");
				$(".quiz1 .questionGroup1").fadeIn(3000);
				$(".quiz1 .quiz-next").removeClass("d-none");
				$(".quiz1 .quiz-next").addClass("d-block");
				
				quizInterval = setInterval(function(){
					timelimit--;
					$(".quiz1 .quizStage h1 a").html("&nbsp;00:" + timeFormat(timelimit));
					if(timelimit == 0) {
						checkedAnswer();
						
						$(".quiz1 .questionGroup" + questionNo).css("display","none");
						showSubmitForm();
					}
					
				}, 1000);
            
			});
			
		});
	});	
	
	function checkedAnswer() {
		
		var user_answer = [];
		$.each($(".quiz1 .questionGroup"+ questionNo).find("input[name^='answer']"), function() {
			user_answer.push($(this).prop('checked'));
        });
		
		for(var i = 0; i < answerLength[questionNo]; i++)
	    	if (correct_answer[questionNo][i]) {
	    		correct_count++;
	    		if(user_answer[i]) answer_count++;
	    	} else {
	    		if(user_answer[i]) {
	    			answer_count--;
		    		if(answer_count < 0) answer_count = 0;
	    		}
	    	}
	}
	
	$(".quiz1 .quiz-next").click(function(e){
		$(".quiz1 .questionGroup" + questionNo).css("display","none");
		
		checkedAnswer();	    
		
		if(questionNo == questions_number) {
			showSubmitForm();
			clearInterval(quizInterval);
		} else {
			questionNo++;
			$(".quiz1 .questionGroup" + questionNo).fadeIn(3000);
			
		}
	});	
	
	$(".quiz1 .submitStage").find("form").submit(function(e) {
		e.preventDefault();
	    var form_action = $(this).attr("action");
	    var user_name = $(this).find("input[name='name']").val();
	    var user_email = $(this).find("input[name='email']").val();
	    
	    $.ajax({
			dataType: 'json',
			type:'POST',
			url: form_action,
			data:{user_name:user_name, user_email:user_email, quiz_id:quiz_id, correct_count:correct_count, answer_count:answer_count}
		}).done(function(data){
			
			$(".quiz1 .card-header a").html("Feedback");
		
			$(".quiz1 .submitStage").removeClass("d-flex");
			$(".quiz1 .submitStage").addClass("d-none");
			
			$(".quiz1 .feedbackStage").removeClass("d-none");
			$(".quiz1 .feedbackStage").addClass("d-block");
			
			$(".quiz1 .feedback").html(user_name + " Feedback");
			$(".quiz1 .answer").html("<h6>" + answer_count + " / " + correct_count + " points </h6>");
			$(".quiz1 .percent").html("<h6>" + (answer_count / correct_count * 100).toFixed(2) + "% </h6>");
			
		});
	});
	
	function showSubmitForm() {
		
		$(".quiz1 .card-header a").html("Submit Quiz");
		
		$(".quiz1 .quizHeader").removeClass("d-block");
		$(".quiz1 .quizHeader").addClass("d-none");
		
		$(".quiz1 .quiz-next").removeClass("d-block");
		$(".quiz1 .quiz-next").addClass("d-none");
		
		$(".quiz1 .submitStage").removeClass("d-none");
		$(".quiz1 .submitStage").addClass("d-flex");
		
	}
	
	//-----------------------------------Quiz 2--------------------------------
	
	$(".quiz2 .startStage").find("form").submit(function(e) {
		
		e.preventDefault();
		questions_number_2 = $(".quiz2 .questions_number").val();
		
		$.ajax({
			url : 'quiz/getRandom',
			type: 'POST',
			dataType: 'json'
		}).done(function(quiz){
			
			var title = quiz.title;
			var image = "uploads/" + quiz.image;
			quiz_id_2 = quiz.id;
			
			$.ajax({
				url : 'question/getRandom',
				type: 'POST',
				dataType: 'json',
				data: {quiz_id:quiz_id_2, questions_number:questions_number_2}
			}).done(function(data){
				
				$(".quiz2 .card-header a").html(title);
				$(".quiz2 .startStage").removeClass("d-flex");
				$(".quiz2 .startStage").addClass("d-none");
				
				var timelimit = 0;
				var	questions = '';
                var cnt = 0;
                
                answerLength_2 = [];
                correct_answer_2 = [];
                
        		$.each( data, function( key, value ) {
        	    	cnt++;
        	    	timelimit += value.time_limit;
        	    	
        	    	var answer = JSON.parse(value.answer);
    				answerLength_2[cnt] = answer.length;			
    				correct_answer_2[cnt] = JSON.parse(value.correct_answer);
    				
    				var rows = `<h5>` + value.question + `</h5>
    					<div class="col-md-8">`;
    				for(var i = 0; i < answerLength_2[cnt]; i++) {            
    					row = `
    						<div class="m-2">
    			            	<input type="checkbox" name="answer` + (i + 1) + `"> ` + answer[i] + `
    			            </div>`;
    			    	rows += row;
    				}    				
    				rows += `</div>`;
    				
        			var question = `
        				<div class="questionGroup` + cnt + `" style="display:none;">` + rows + `
        	        	</div>`;
                    questions += question;
        		});
        		if (cnt < questions_number_2) questions_number_2 = cnt;
        		
        		var header = `
					<div class="text-center question-bell">
			           	<h1><i class="fas fa-bell"></i><a>` + "&nbsp;00:" + timeFormat(timelimit) + `</a></h1>
			        </div>
			        <div style="text-align:center;width:100px;">
						<img src='` + image + `' style = "width:100%;">
					</div>`;
            	$(".quiz2 .quizHeader").html(header);
            	
            	$('.quiz2 .quizBody').html(questions);
				
				$(".quiz2 .quizStage").removeClass("d-none");
				$(".quiz2 .quizStage").addClass("d-block");
				$(".quiz2 .questionGroup1").fadeIn(3000);
				$(".quiz2 .quiz-next").removeClass("d-none");
				$(".quiz2 .quiz-next").addClass("d-block");
				
				quizInterval_2 = setInterval(function(){
					timelimit--;
					$(".quiz2 .quizStage h1 a").html("&nbsp;00:" + timeFormat(timelimit));
					if(timelimit == 0) {
						checkedAnswer_2();
						
						$(".quiz2 .questionGroup" + questionNo_2).css("display","none");
						showSubmitForm_2();
					}
					
				}, 1000);
            
			});
			
		});
	});	
	
	function checkedAnswer_2() {
		
		var user_answer = [];
		$.each($(".quiz2 .questionGroup"+ questionNo_2).find("input[name^='answer']"), function() {
			user_answer.push($(this).prop('checked'));
        });
		
		for(var i = 0; i < answerLength_2[questionNo_2]; i++)
	    	if (correct_answer_2[questionNo_2][i]) {
	    		correct_count_2++;
	    		if(user_answer[i]) answer_count_2++;
	    	} else {
	    		if(user_answer[i]) {
	    			answer_count_2--;
		    		if(answer_count_2 < 0) answer_count_2 = 0;
	    		}
	    	}
	}
	
	$(".quiz2 .quiz-next").click(function(e){
		$(".quiz2 .questionGroup" + questionNo_2).css("display","none");
		
		checkedAnswer_2();	    
		
		if(questionNo_2 == questions_number_2) {
			showSubmitForm_2();
			clearInterval(quizInterval_2);
		} else {
			questionNo_2++;
			$(".quiz2 .questionGroup" + questionNo_2).fadeIn(3000);
			
		}
	});	
	
	$(".quiz2 .submitStage").find("form").submit(function(e) {
		e.preventDefault();
	    var form_action = $(this).attr("action");
	    var user_name = $(this).find("input[name='name']").val();
	    var user_email = $(this).find("input[name='email']").val();
	    
	    $.ajax({
			dataType: 'json',
			type:'POST',
			url: form_action,
			data:{user_name:user_name, user_email:user_email, quiz_id:quiz_id_2, correct_count:correct_count_2, answer_count:answer_count_2}
		}).done(function(data){
			
			$(".quiz2 .card-header a").html("Feedback");
		
			$(".quiz2 .submitStage").removeClass("d-flex");
			$(".quiz2 .submitStage").addClass("d-none");
			
			$(".quiz2 .feedbackStage").removeClass("d-none");
			$(".quiz2 .feedbackStage").addClass("d-block");
			
			$(".quiz2 .feedback").html(user_name + " Feedback");
			$(".quiz2 .answer").html("<h6>" + answer_count_2 + " / " + correct_count_2 + " points </h6>");
			$(".quiz2 .percent").html("<h6>" + (answer_count_2 / correct_count_2 * 100).toFixed(2) + "% </h6>");
			
		});
	});
	
	function showSubmitForm_2() {
		
		$(".quiz2 .card-header a").html("Submit Quiz");
		
		$(".quiz2 .quizHeader").removeClass("d-block");
		$(".quiz2 .quizHeader").addClass("d-none");
		
		$(".quiz2 .quiz-next").removeClass("d-block");
		$(".quiz2 .quiz-next").addClass("d-none");
		
		$(".quiz2 .submitStage").removeClass("d-none");
		$(".quiz2 .submitStage").addClass("d-flex");
		
	}
	
	function timeFormat(time) {
		if(time < 10)
			return "0" + time;
		else
			return time;
	}
	
});

</script>
</body>

</html>
