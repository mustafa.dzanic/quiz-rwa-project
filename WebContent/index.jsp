
<jsp:include page="layouts/header.jsp" />

<div class="container mt-3">
	<div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
				<div class="card-header">
                    <a class="align-middle">Start Quiz</a>
                    <button class="btn btn-info float-right d-none quiz-next" title="Next"><i class="fas fa-arrow-right"></i> Next</button>
				</div>
				<div class="card-body">
					<div id="startStage" class="d-flex justify-content-center">
                        <form class="col-md-3 text-center">
	                        <div class="form-group">
								<label for="questions_number">Enter number of questions: </label>
								<input type="number" id="questions_number" class="form-control" name="questions_number" min=1 max=25 value=2 required></input>
                            </div>
                            <div class="form-group">
	                            <button type="submit" class="btn btn-default border play-submit" style="width:100px;">
	                            	<i class="fas fa-play"></i>&nbsp;&nbsp;Play
	                            </button>
                            </div>
                        </form>
	                </div>
	                <div id="quizStage" class="d-none p-1">
	                    <div id="quizHeader" class="mt-2">	                    
	                    </div>
	                    <div id="quizBody" class="mt-2">
	                    </div>
	                </div>
	                <div id="submitStage" class="justify-content-center d-none p-3">
	                    <form class="col-md-6" action="quizresult/create">
							<div class="form-group">
								<label for="name">Name:</label>
								<input type="text" class="form-control" placeholder="Enter name" name="name" required>
							</div>							
							<div class="form-group">
								<label for="email">E-Mail Address:</label>
								<input type="text" class="form-control" placeholder="Enter email address" name="email" required>
							</div>						
							<div class="form-group">
								<button type="submit" class="btn btn-primary quiz-submit">Submit</button>
							</div>							
						</form>
	                </div>
	                <div id="feedbackStage" class="text-center d-none p-3">
	                    <h4 id="feedback"></h4>
	                    <div id="answer"></div>
	                    <div id="percent"></div>
	                </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Scripts -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.5/validator.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

<script src="https://use.fontawesome.com/releases/v5.8.1/js/solid.js" ></script>
<script src="https://use.fontawesome.com/releases/v5.8.1/js/fontawesome.js"></script>

<script>

$( document ).ready(function() {
	
	var quiz_id;
	var questionNo = 1;
	var questions_number;
	
	var answerLength;
	var correct_answer;
	var user_answer;
	
	var correct_count = 0;
	var answer_count = 0;
	
	var quizInterval;
	
	$("#startStage").find("form").submit(function(e) {
		
		e.preventDefault();
		questions_number = $("#questions_number").val();
		
		$.ajax({
			url : 'quiz/getRandom',
			type: 'POST',
			dataType: 'json'
		}).done(function(quiz){
			
			var title = quiz.title;
			var image = "uploads/" + quiz.image;
			quiz_id = quiz.id;
			
			$.ajax({
				url : 'question/getRandom',
				type: 'POST',
				dataType: 'json',
				data: {quiz_id:quiz_id, questions_number:questions_number}
			}).done(function(data){
				
				$(".card-header a").html(title);
				$("#startStage").removeClass("d-flex");
				$("#startStage").addClass("d-none");
				
				var timelimit = 0;
				var	questions = '';
                var cnt = 0;
                
                answerLength = [];
                correct_answer = [];
                
        		$.each( data, function( key, value ) {
        	    	cnt++;
        	    	timelimit += value.time_limit;
        	    	
        	    	var answer = JSON.parse(value.answer);
    				answerLength[cnt] = answer.length;			
    				correct_answer[cnt] = JSON.parse(value.correct_answer);
    				
    				rows = `<h5>` + value.question + `</h5>
    					<div class="col-md-8">`;
    				for(var i = 0; i < answerLength[cnt]; i++) {            
    					row = `
    						<div class="m-2">
    			            	<input type="checkbox" name="answer` + (i + 1) + `"> ` + answer[i] + `
    			            </div>`;
    			    	rows += row;
    				}    				
    				rows += `</div>`;
    				
        			var question = `
        				<div id="questionGroup` + cnt + `" style="display:none;">` + rows + `
        	        	</div>`;
                    questions += question;
        		});
        		if (cnt < questions_number) questions_number = cnt;
        		
        		var header = `
					<div class="text-center question-bell">
			           	<h1><i class="fas fa-bell"></i><a>` + "&nbsp;00:" + timeFormat(timelimit) + `</a></h1>
			        </div>
			        <div style="text-align:center;width:250px;">
						<img src='` + image + `' style = "width:100%;">
					</div>`;
            	$("#quizHeader").html(header);
            	
            	$('#quizBody').html(questions);
				
				$("#quizStage").removeClass("d-none");
				$("#quizStage").addClass("d-block");
				$("#questionGroup1").fadeIn(3000);
				$(".quiz-next").removeClass("d-none");
				$(".quiz-next").addClass("d-block");
				
				quizInterval = setInterval(function(){
					timelimit--;
					$("#quizStage h1 a").html("&nbsp;00:" + timeFormat(timelimit));
					if(timelimit == 0) {
						checkedAnswer();
						
						$("#questionGroup" + questionNo).css("display","none");
						showSubmitForm();
					}
					
				}, 1000);
            
			});
			
		});
	});	
	
	function checkedAnswer() {
		
		var user_answer = [];
		$.each($("#questionGroup"+ questionNo).find("input[name^='answer']"), function() {
			user_answer.push($(this).prop('checked'));
        });
		
		for(var i = 0; i < answerLength[questionNo]; i++)
	    	if (correct_answer[questionNo][i]) {
	    		correct_count++;
	    		if(user_answer[i]) answer_count++;
	    	} else {
	    		if(user_answer[i]) {
	    			answer_count--;
		    		if(answer_count < 0) answer_count = 0;
	    		}
	    	}
	}
	
	$(".quiz-next").click(function(e){
		$("#questionGroup" + questionNo).css("display","none");
		
		checkedAnswer();	    
		
		if(questionNo == questions_number) {
			showSubmitForm();
			clearInterval(quizInterval);
		} else {
			questionNo++;
			$("#questionGroup" + questionNo).fadeIn(3000);
			
		}
	});	
	
	$("#submitStage").find("form").submit(function(e) {
		e.preventDefault();
	    var form_action = $(this).attr("action");
	    var user_name = $(this).find("input[name='name']").val();
	    var user_email = $(this).find("input[name='email']").val();
	    
	    $.ajax({
			dataType: 'json',
			type:'POST',
			url: form_action,
			data:{user_name:user_name, user_email:user_email, quiz_id:quiz_id, correct_count:correct_count, answer_count:answer_count}
		}).done(function(data){
			
			$(".card-header a").html("Feedback");
		
			$("#submitStage").removeClass("d-flex");
			$("#submitStage").addClass("d-none");
			
			$("#feedbackStage").removeClass("d-none");
			$("#feedbackStage").addClass("d-block");
			
			$("#feedback").html(user_name + " Feedback");
			$("#answer").html("<h6>" + answer_count + " / " + correct_count + " points </h6>");
			$("#percent").html("<h6>" + (answer_count / correct_count * 100).toFixed(2) + "% </h6>");
			
		});
	});
	
	function showSubmitForm() {
		
		$(".card-header a").html("Submit Quiz");
		
		$("#quizHeader").removeClass("d-block");
		$("#quizHeader").addClass("d-none");
		
		$(".quiz-next").removeClass("d-block");
		$(".quiz-next").addClass("d-none");
		
		$("#submitStage").removeClass("d-none");
		$("#submitStage").addClass("d-flex");
		
	}
	
	function timeFormat(time) {
		if(time < 10)
			return "0" + time;
		else
			return time;
	}
	
});

</script>
</body>

</html>
