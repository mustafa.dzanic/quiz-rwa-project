<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="">
    <meta name="author" content="">
    
	<title> Quiz Site </title>
    
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">

	<!-- main style.css -->
    <!-- <link rel="stylesheet" href="assets/css/index.css">  -->

    <style>
	    body {
		    background:#fafafa;
		}
		.navbar {
		    background:white;
		}
		#user img{
			width:80px;
			height:80px;
			border-radius: 50%;
		}
		.navbar img{
			width:40px;
			height:40px;
			border-radius: 50%;
		}
		.quiz-start {
			line-height:40px;
		}
		.footer {
		    position: absolute;
		    bottom: 0;
		    width: 100%;
		    height: 40px;
		    background-color: #ffffff;
		}
    </style>
    
</head>
<body>

<nav class="navbar navbar-expand-md navbar-light navbar-laravel">
	<div class = "container">
		<a class="navbar-brand" href="index.jsp">
			<h3>Quiz</h3>
		</a>
		<ul class="navbar-nav ml-auto">
			<li class="nav-item"><a class="nav-link" href="index2.jsp">Quiz2</a></li>
		</ul>
		<ul class="navbar-nav ml-auto">
		
		<%  //if (session != null) { %>
		<%  if (session.getAttribute("email") == null) { %>
			<li class="nav-item">
				<a class="nav-link" href="login.jsp">Login</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="signup.jsp">Sign up</a>
			</li>
		<% //} else if (session != null && session.getAttribute("email") != null) { %>
		<% } else { %>
			<li class="nav-item dropdown">
				<a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<% out.print(session.getAttribute("name")); %>
				</a>
				<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
					<% if (session.getAttribute("usertype").equals("admin")) { %>
					<a class="dropdown-item" href="user.jsp">
						<i class = "fas fa-user-edit"></i> User Manage
					</a>
					<% } %>
					<a class="dropdown-item" href="quiz.jsp">
						<i class = "fas fa-clipboard"></i> Quiz Manage
					</a>
					<a class="dropdown-item" href="inbox.jsp">
						<i class="fas fa-inbox"></i> Inbox Manage
					</a>
					<a class="dropdown-item" href="logout">
						<i class = "fas fa-sign-out-alt"></i> Logout
					</a>
				</div>
			</li>
		<% } %>
		</ul>
		
	</div>
</nav>


<% if (session.getAttribute("email") != null && session.getAttribute("usertype").equals("admin")) { %>
	<input class = "usertype " type = "hidden" value = "admin" />
<% } %>