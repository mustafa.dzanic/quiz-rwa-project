<jsp:include page="layouts/header.jsp" />

<% if (session.getAttribute("email") == null || !session.getAttribute("usertype").equals("admin")) { 
	response.sendRedirect("login.jsp");
} %>

<div class="container mt-3">
	<div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
				<div class="card-header">
                    User Manage
                </div>
				<div class="card-body">
					<div class="p-0 mb-2">
						<button class="btn btn-danger" data-toggle="modal" data-target="#create-item">
							<i class="fas fa-plus mr-2"></i>NEW USER
						</button>
					</div>
					<table id = "user" class="table text-center">
						<thead>
							<tr>
								<th style="width:10%;">No</th>
								<th style="width:25%;">Name</th>
								<th style="width:30%;">E-Mail Address</th>
								<th style="width:15%;">Password</th>
								<th style="width:20%;">Action</th>								
							</tr>
						</thead>
						<tbody>
						</tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
	
</div>

<!-- Create Item Modal -->
<div class="modal" id="create-item">
	<div class="modal-dialog">
		<div class="modal-content">

			<!-- Modal Header -->
			<div class="modal-header">
				<h4 class="modal-title">Create A New User</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>

			<!-- Modal body -->
			<div class="modal-body">
				<form action="user/create">
					
					<div class="form-group">
						<label for="name">Name:</label>
						<input type="text" class="form-control" placeholder="Enter name" name="name" required>
					</div>
					
					<div class="form-group">
						<label for="email">E-Mail Address:</label>
						<input type="text" class="form-control" placeholder="Enter email address" name="email" required>
					</div>
					
					<div class="form-group">
						<label for="password">Password:</label>
						<input type="password" class="form-control" placeholder="Enter password" name="password" minlength="6" required>
					</div>
					
					<div class="form-group">
						<button type="submit" class="btn btn-primary create-submit">Submit</button>
					</div>
					
				</form>
			</div>

			<!-- Modal footer -->
			<div class="modal-footer">
				
			</div>

		</div>
	</div>
</div>

<!-- Edit Item Modal -->
<div class="modal" id="edit-item">
	<div class="modal-dialog">
		<div class="modal-content">

			<!-- Modal Header -->
			<div class="modal-header">
				<h4 class="modal-title">Edit A User</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>

			<!-- Modal body -->
			<div class="modal-body">

				<form action="user/update">
					<input type = "hidden" class = "edit-id" />
					<div class="form-group">
						<label for="name">Name:</label>
						<input type="text" class="form-control" placeholder="Enter name" name="name" required>
					</div>
					
					<div class="form-group">
						<label for="email">E-Mail Address:</label>
						<input type="text" class="form-control" placeholder="Enter email address" name="email" required>
					</div>
					
					<div class="form-group">
						<label for="password">Password:</label>
						<input type="password" class="form-control" placeholder="Enter password" name="password" minlength="6" required>
					</div>
					
					<div class="form-group">
						<button type="submit" class="btn btn-primary edit-submit">Submit</button>
					</div>
					
				</form>
			</div>

			<!-- Modal footer -->
			<div class="modal-footer">
				
			</div>

		</div>
	</div>
</div>
<!-- Scripts -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.5/validator.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

<script src="https://use.fontawesome.com/releases/v5.8.1/js/solid.js" ></script>
<script src="https://use.fontawesome.com/releases/v5.8.1/js/fontawesome.js"></script>

<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.9/validator.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>

<!-- <script src="assets/js/user.js"></script>  -->

<script>

$( document ).ready(function() {
	
    var altTimeOut = 2000;
	getUsers();

	function getUsers() {
		$.ajax({
			type:'POST',
	    	url: 'user/list'
		}).done(function(data){
			manageRow(data);
	    	$('#user').DataTable();
		});
	}

	function manageRow(data) {
		var	rows = '';
        var cnt = 0;
        
		$.each( data, function( key, value ) {
	    	
			/*if (value.photo)
            	imgTag = "<img src=" + value.photo + " />";
            else
            	imgTag = "<img src='https://colorlib.com/wp/wp-content/uploads/avatar/0.svg' />";*/
			
            cnt++;
            var row = `
	        	<tr data-id = "` + value.id + `">
	            <td>` + cnt + `</td>
	            <td>` + value.name + `</td>
	            <td>` + value.email + `</td>
	            <td>` + "*******" + `</td>
	            <td>
		            <button class = "btn btn-info mr-2 edit-item" data-toggle="modal" data-target="#edit-item"><i class='fas fa-pen-alt'></i></button>
	            	<button class = "btn btn-danger delete-item"><i class='fas fa-trash-alt'></i></button>
	            </td>
            </tr>`;
	    	rows = rows + row;
		});

		$("tbody").html(rows);
	}
	

	/* Create new Item */
	$("#create-item").find("form").submit(function(e) {
	    e.preventDefault();
	    var form_action = $(this).attr("action");
	    var name = $(this).find("input[name='name']").val();
	    var email = $(this).find("input[name='email']").val();
	    var password = $(this).find("input[name='password']").val();
	    
		$.ajax({
			dataType: 'json',
			type:'POST',
			url: form_action,
			data:{name:name, email:email, password:password}
		}).done(function(data){
			if (data == true) {
				$(this).find("input[name='name']").val('');
				$(this).find("input[name='email']").val('');
				$(this).find("input[name='password']").val('');
				
				getUsers();
				$(".modal").modal('hide');
				toastr.success('Created Successfully.', 'Success Alert', {timeOut: altTimeOut});
			} else {
				toastr.error('Failed to create.', 'Failed Alert', {timeOut: altTimeOut});
			}
		});

	});

	/* Edit Item */
	$("body").on("click",".edit-item",function(){

	    var tr = $(this).closest("tr");
	    
	    var id = tr.data('id');
	    var name = tr.find("td:eq(1)").text();
	    var email = tr.find("td:eq(2)").text();	    
	    var password = tr.find("td:eq(3)").text();	    
    	
    	$("#edit-item").find("input[name='name']").val(name);
	    $("#edit-item").find("input[name='email']").val(email);
	    $("#edit-item").find("input[name='password']").val(password);
	    $("#edit-item").find(".edit-id").val(id);

	});
	
	/* Update new Item */
	$(".edit-submit").click(function(e){

	    e.preventDefault();
	    var form_action = $("#edit-item").find("form").attr("action");
	    var id = $("#edit-item").find(".edit-id").val();
		var name = $("#edit-item").find("input[name='name']").val();
		var email = $("#edit-item").find("input[name='email']").val();
		var password = $("#edit-item").find("input[name='password']").val();
	    console.log(id + name + email + password);
		$.ajax({
			dataType: 'json',
			type:'POST',
			url: form_action,
			data:{id:id, name:name, email:email, password:password}
		}).done(function(data){
			if (data == true) {
				getUsers();
				$(".modal").modal('hide');
				toastr.success('Updated Successfully.', 'Success Alert', {timeOut: altTimeOut});
			} else {
				toastr.error('Failed to update.', 'Failed Alert', {timeOut: altTimeOut});
			}
		});

	});
	
	$("body").on("click",".delete-item",function(){
		var id = $(this).closest("tr").data("id");
		
		$.ajax({
			dataType: 'json',
			type:'POST',
	    	url: 'user/delete',
	    	data:{id:id}
		}).done(function(data){
			if (data == true) {
				getUsers();
				toastr.success('Deleted Successfully.', 'Success Alert', {timeOut: altTimeOut});
			} else {
				toastr.error('Failed to delete.', 'Failed Alert', {timeOut: altTimeOut});
			}
		});
	});

});

</script>

</body>

</html>
