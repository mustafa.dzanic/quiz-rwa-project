<jsp:include page="layouts/header.jsp" />

<% if (session.getAttribute("email") == null) { 
	response.sendRedirect("login.jsp");
} %>

<div class="container mt-3">
	<div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
				<div class="card-header">
                    Inbox Manage
					<button class="btn btn-default float-right border download-csv">
						<i class="fas fa-download"></i> Download
					</button>
                    <button class="btn btn-default float-right border convert-csv mr-2">
						<i class="fas fa-angle-double-right"></i> Convert to CSV
					</button>
                </div>
				<div class="card-body">
					<table id = "quizresult" class="table text-center">
						<thead>
							<tr>
								<th style="width:5%;">No</th>
								<th style="width:10%;">Name</th>
								<th style="width:10%;">E-mail</th>
								<th style="width:30%;">Quiz Title</th>
								<th style="width:10%;">Answer(Correct/Total)</th>								
								<th style="width:15%;">Date</th>								
								<th style="width:20%;">Action</th>								
							</tr>
						</thead>
						<tbody>
						</tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
	
</div>

<!-- Scripts -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.5/validator.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

<script src="https://use.fontawesome.com/releases/v5.8.1/js/solid.js" ></script>
<script src="https://use.fontawesome.com/releases/v5.8.1/js/fontawesome.js"></script>

<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.9/validator.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>

<!-- <script src="assets/js/quizresult.js"></script>  -->

<script>

$( document ).ready(function() {
	
    var altTimeOut = 2000;
    
	getQuizResults();

	function getQuizResults() {
		$.ajax({
			type:'POST',
	    	url: 'quizresult/list'
		}).done(function(data){
			manageRow(data);
	    	$('#quizresult').DataTable();
		});
	}

	function manageRow(data) {
		var	rows = '';
        var cnt = 0;
        
		$.each( data, function( key, value ) {
	    	cnt++;
			var row = `<tr data-id = "` + value.id + `">
	        	<td>` + cnt + `</td>
	        	<td>` + value.user_name + `</td>
	        	<td>` + value.user_email + `</td>
	        	<td>` + value.quiz_title + `</td>
	        	<td>` + value.answer_count + "/" + value.correct_count + `</td>
	        	<td>` + value.create_at + `</td>
	            <td>
	            	<button class = "btn btn-danger delete-item"><i class='fas fa-trash-alt'></i></button>
	            </td>
            </tr>`;
	    	rows = rows + row;
		});

		$("tbody").html(rows);
	}
	
	$("body").on("click",".convert-csv",function(){
		$.ajax({
	    	url: 'quizresult/convertCSV',
			type:'POST'
		}).done(function(data){
			if (data == true) {
				toastr.success('Converted into the folder csv', 'Success Alert', {timeOut: altTimeOut});
			} else {
				toastr.error('Failed to convert.', 'Failed Alert', {timeOut: altTimeOut});
			}
		});
	});
	
	$("body").on("click",".download-csv",function(){
		location.href = "quizresult/downloadCSV";
	});	

	$("body").on("click",".delete-item",function(){
		var id = $(this).closest("tr").data("id");
		
		$.ajax({
			dataType: 'json',
			type:'POST',
	    	url: 'quizresult/delete',
	    	data:{id:id}
		}).done(function(data){
			if (data == true) {
				getQuizResults();
				toastr.success('Deleted Successfully.', 'Success Alert', {timeOut: altTimeOut});
			} else {
				toastr.error('Failed to delete.', 'Failed Alert', {timeOut: altTimeOut});
			}
		});
	});

});

</script>

</body>

</html>
